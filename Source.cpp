#include "XLibrary11.hpp"
using namespace XLibrary;

enum BlockType
{
    None,
    Red,
    Blue,
    Yellow,
    Green,
    Wall,
};

enum GameState
{
    Init,
    Move,
    Delete,
    Fall,
};

enum PuyoOrientation
{
    Up,
    Down,
    Left,
    Right,
};

const int WIDTH = 8;
const int HEIGHT = 15;

BlockType board[WIDTH][HEIGHT];

GameState state = Init;

Sprite puyoSprite(L"puyo.png");
Sprite wallSprite(L"wall.png");

XMINT2 movePosition;
PuyoOrientation moveOrientation;
BlockType moveColor[2];

float timer = 0.0f;

void InitMovePuyo()
{
    movePosition = XMINT2(3, HEIGHT - 2);
    moveOrientation = Right;
    moveColor[0] = (BlockType)Random::Range(Red, Green);
    moveColor[1] = (BlockType)Random::Range(Red, Green);
}

void InitBoard()
{
    puyoSprite.scale = 2.0f;
    wallSprite.scale = 2.0f;

    for (int x = 0; x < WIDTH; x++)
    {
        for (int y = 0; y < HEIGHT; y++)
        {
            board[x][y] = None;
        }
    }

    for (int x = 0; x < WIDTH; x++)
    {
        board[x][0] = Wall;
    }

    for (int y = 0; y < HEIGHT; y++)
    {
        board[0][y] = Wall;
        board[WIDTH - 1][y] = Wall;
    }

    InitMovePuyo();

    timer += Timer::GetDeltaTime();
    if (timer > 1.0f)
    {
        timer = 0.0f;
        state = Move;
    }
}

XMINT2 GetMovePuyoPosition(XMINT2 movePosition)
{
    XMINT2 movePosition2 = movePosition;
    switch (moveOrientation)
    {
    case Up:
        movePosition2.y++;
        break;

    case Down:
        movePosition2.y--;
        break;

    case Left:
        movePosition2.x--;
        break;

    case Right:
        movePosition2.x++;
        break;
    }

    return movePosition2;
}

void MovePuyo()
{
    XMINT2 movePosition2 = GetMovePuyoPosition(movePosition);

    board[movePosition.x][movePosition.y] = None;
    board[movePosition2.x][movePosition2.y] = None;

    if (Input::GetKeyDown(VK_SPACE))
    {
        switch (moveOrientation)
        {
        case Up:
            if (board[movePosition.x + 1][movePosition.y] == None)
                moveOrientation = Right;
            break;

        case Down:
            if (board[movePosition.x - 1][movePosition.y] == None)
                moveOrientation = Left;
            break;

        case Left:
            moveOrientation = Up;
            break;

        case Right:
            moveOrientation = Down;
            break;
        }
        movePosition2 = GetMovePuyoPosition(movePosition);
    }

    if (Input::GetKeyDown(VK_LEFT))
    {
        if (board[movePosition.x - 1][movePosition.y] == None &&
            board[movePosition2.x - 1][movePosition2.y] == None)
        {
            movePosition.x--;
            movePosition2.x--;
        }
    }

    if (Input::GetKeyDown(VK_RIGHT))
    {
        if (board[movePosition.x + 1][movePosition.y] == None &&
            board[movePosition2.x + 1][movePosition2.y] == None)
        {
            movePosition.x++;
            movePosition2.x++;
        }
    }

    float wait = 0.5f;
    if (Input::GetKey(VK_DOWN))
        wait = 0.1f;

    timer += Timer::GetDeltaTime();
    if (timer > wait)
    {
        movePosition.y--;
        movePosition2.y--;
        timer = 0.0f;
    }

    board[movePosition.x][movePosition.y] = moveColor[0];
    board[movePosition2.x][movePosition2.y] = moveColor[1];

    if (board[movePosition.x][movePosition.y - 1] != None &&
        movePosition.y - 1 != movePosition2.y)
    {
        InitMovePuyo();
        timer = 0.0f;
        state = Fall;
    }

    if (board[movePosition2.x][movePosition2.y - 1] != None &&
        movePosition2.y - 1 != movePosition.y)
    {
        InitMovePuyo();
        timer = 0.0f;
        state = Fall;
    }
}

void FallPuyo()
{
    timer += Timer::GetDeltaTime();
    if (timer < 0.5f)
    {
        return;
    }

    for (int x = 0; x < WIDTH; x++)
    {
        if (board[x][HEIGHT - 2] != None &&
            board[x][HEIGHT - 2] != Wall)
        {
            state = Init;
            return;
        }
    }

    bool isFalling = false;

    for (int x = 0; x < WIDTH; x++)
    {
        for (int y = 0; y < HEIGHT; y++)
        {
            if (board[x][y] == Red ||
                board[x][y] == Blue ||
                board[x][y] == Yellow ||
                board[x][y] == Green)
            {
                if (board[x][y - 1] == None)
                {
                    board[x][y - 1] = board[x][y];
                    board[x][y] = None;
                    isFalling = true;
                }
            }
        }
    }

    if (!isFalling)
    {
        timer = 0.0f;
        state = Delete;
    }
}

int SearchLinkPuyo(int x, int y, bool searched[WIDTH][HEIGHT], int linkNum[WIDTH][HEIGHT])
{
    if (x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT)
        return 0;

    if (board[x][y] == None ||
        board[x][y] == Wall)
        return 0;

    if (searched[x][y])
        return 0;

    searched[x][y] = true;
    int link = 1;

    if (board[x][y] == board[x - 1][y])
        link += SearchLinkPuyo(x - 1, y, searched, linkNum);
    if (board[x][y] == board[x + 1][y])
        link += SearchLinkPuyo(x + 1, y, searched, linkNum);
    if (board[x][y] == board[x][y - 1])
        link += SearchLinkPuyo(x, y - 1, searched, linkNum);
    if (board[x][y] == board[x][y + 1])
        link += SearchLinkPuyo(x, y + 1, searched, linkNum);

    linkNum[x][y] = link;
    return link;
}

void DeleteLinkPuyo(int x, int y, BlockType type, int linkNum[WIDTH][HEIGHT])
{
    if (x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT)
        return;

    if (board[x][y] != type)
        return;

    board[x][y] = None;
    linkNum[x][y] = 0;
    DeleteLinkPuyo(x - 1, y, type, linkNum);
    DeleteLinkPuyo(x + 1, y, type, linkNum);
    DeleteLinkPuyo(x, y - 1, type, linkNum);
    DeleteLinkPuyo(x, y + 1, type, linkNum);
}

void DeletePuyo()
{
    timer += Timer::GetDeltaTime();
    if (timer < 0.5f)
    {
        return;
    }

    bool searched[WIDTH][HEIGHT] = {};
    int linkNum[WIDTH][HEIGHT] = {};

    for (int x = 0; x < WIDTH; x++)
    {
        for (int y = 0; y < HEIGHT; y++)
        {
            SearchLinkPuyo(x, y, searched, linkNum);
        }
    }

    bool isDelete = false;

    for (int x = 0; x < WIDTH; x++)
    {
        for (int y = 0; y < HEIGHT; y++)
        {
            if (linkNum[x][y] >= 4)
            {
                DeleteLinkPuyo(x, y, board[x][y], linkNum);
                isDelete = true;
            }
        }
    }

    timer = 0.0f;

    if (isDelete)
        state = Fall;
    else
        state = Move;
}

void DrawBoard()
{
    for (int x = 0; x < WIDTH; x++)
    {
        for (int y = 0; y < HEIGHT; y++)
        {
            puyoSprite.position = Float3(x * 32.0f, y * 32.0f, 0);

            switch (board[x][y])
            {
            case Red:
                puyoSprite.color = Float4(1.0f, 0.0f, 0.0f, 1.0f);
                puyoSprite.Draw();
                break;

            case Blue:
                puyoSprite.color = Float4(0.0f, 0.5f, 1.0f, 1.0f);
                puyoSprite.Draw();
                break;

            case Yellow:
                puyoSprite.color = Float4(1.0f, 1.0f, 0.0f, 1.0f);
                puyoSprite.Draw();
                break;

            case Green:
                puyoSprite.color = Float4(0.0f, 1.0f, 0.5f, 1.0f);
                puyoSprite.Draw();
                break;

            case Wall:
                wallSprite.position = Float3(x * 32.0f, y * 32.0f, 0);
                wallSprite.Draw();
                break;
            }
        }
    }
}

int MAIN()
{
    Window::SetSize(WIDTH * 32, (HEIGHT - 2) * 32);
    Camera camera;
    camera.position = Float3((WIDTH / 2.0f - 0.5f) * 32, ((HEIGHT - 2) / 2.0f - 0.5f) * 32, 0.0f);
    camera.color = Float4(0.0f, 0.0f, 0.0f, 0.0f);

    while (Refresh())
    {
        camera.Update();

        switch (state)
        {
        case Init:
            InitBoard();
            break;

        case Move:
            MovePuyo();
            break;

        case Fall:
            FallPuyo();
            break;

        case Delete:
            DeletePuyo();
            break;
        }

        DrawBoard();
    }

    return 0;
}
